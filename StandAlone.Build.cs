﻿using UnrealBuildTool;
using System.IO;
 
public class StandAlone : ModuleRules
{
    public StandAlone(ReadOnlyTargetRules Target) : base(Target)
    {
        PrivateIncludePaths.AddRange(new string[] { "StandAlone/Private" });
        PublicIncludePaths.AddRange(new string[] { "StandAlone/Public" });
 
        PublicDependencyModuleNames.AddRange(new string[] { "Engine", "Core" });
    }
}