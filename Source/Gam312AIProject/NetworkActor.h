// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "NetworkActor.generated.h"

UCLASS()
class GAM312AIPROJECT_API ANetworkActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties, 
	//had to rename to OwnerCust as Owner was previously 
	//declared in AActor which is read only and cannot be overwridden
	//I believe if the value of this variable is changed, it should change in all sessions connected to the server.
	//I'm looking forward to trying this out !
	UPROPERTY(replicated)
		AActor* OwnerCust;


	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const override;

protected:

	//replaced FPostConstructInitializeProperties with FObjectInitializer, which is its new name
	ANetworkActor(const class FObjectInitializer & PCIP);
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
};


