// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "MyHUD.generated.h"

/**
 * 
 */
UCLASS()
class GAM312AIPROJECT_API AMyHUD : public AHUD
{
	GENERATED_BODY()
	
public:
	//This adds the HudFont UPROPERTY, so that the font can be edited in the blueprint
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUDFont)
	UFont* hudFont;
	//Overrides the DrawHUD() function
	virtual void DrawHUD() override;
	
};
