// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "Enemy.h"
#include "JoyPathFollowComp.h"//Added reference to cusom path follow component
//Add the custom follow path component and the following to the constructor :

//added constructor which tells AI controller to use custom path follow comp
AMyAIController::AMyAIController()//const FObjectInitializer& ObjectInitializer)
	//: Super(ObjectInitializer.SetDefaultSubobjectClass<UJoyPathFollowComp>(TEXT("PathFollowingComponent")))
{
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));

}

//Possesses the enemy pawn with the AI controller
void AMyAIController::Possess(APawn *InPawn) {
	Super::Possess(InPawn);

	AEnemy *Char = Cast<AEnemy>(InPawn);
	
	//Verifies the char exists, and that the botbehavior exists, to avoid null ref exception
	if (Char && Char->BotBehavior) 
	{
		//Starts the blackboard component with the blackboard asset held by the char's bot behavior
		BlackboardComp->InitializeBlackboard(*Char->BotBehavior->BlackboardAsset);

		//Gets the Key and sets it to hold the player
		EnemyKeyID = BlackboardComp->GetKeyID("Target");

		//starts the behavior tree associated with the character
		BehaviorComp->StartTree(*Char->BotBehavior);
	}
}