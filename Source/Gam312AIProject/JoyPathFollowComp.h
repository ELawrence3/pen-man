// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Navigation/PathFollowingComponent.h"
#include "JoyPathFollowComp.generated.h"

/**
 * 
 */
UCLASS()
class GAM312AIPROJECT_API UJoyPathFollowComp : public UPathFollowingComponent
{
	GENERATED_BODY()
	
	

		//Overridden functions
	/** follow current path segment */
	virtual void FollowPathSegment(float DeltaTime) override;
	/** sets variables related to current move segment */
	virtual void SetMoveSegment(int32 SegmentStartIndex) override;
	/** check state of path following, update move segment if needed */
	virtual void UpdatePathSegment() override;
	
};
