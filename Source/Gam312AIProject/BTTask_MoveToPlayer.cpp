// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_MoveToPlayer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "MyAIController.h"
#include "Penman.h"
#include "Enemy.h"
#include "AIPatrolPoint.h"
#include "BTService_CheckForPlayer.h"


EBTNodeResult::Type UBTTask_MoveToPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) 
{
	//Get a reference to the AI controller attached to parent
	AMyAIController *CharPC = Cast<AMyAIController>(OwnerComp.GetAIOwner());

	//Gets a reference to the player
	AActor *Enemy = Cast<AActor>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));
	//AAIPatrolPoint *Enemy = Cast<AAIPatrolPoint>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->EnemyKeyID));

	//Verifies the player exists to avboid null ref
	if(Enemy){
		//Move towards the player
		CharPC->MoveToActor(Enemy, 5.f, true, true, true, 0, true);

		return EBTNodeResult::Succeeded;
	}
	else {
		return EBTNodeResult::Failed;
	}
	EBTNodeResult::Failed;

}
