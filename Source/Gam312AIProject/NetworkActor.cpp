// Fill out your copyright notice in the Description page of Project Settings.

#include "NetworkActor.h"
#include "Net/UnrealNetwork.h"


// Sets default values
ANetworkActor::ANetworkActor(const class FObjectInitializer & PCIP) :Super(PCIP)
{
	//Sets the default state, indicating this will be replicated
	bReplicates = true;
 	// Set this actor to call Tick() every frame.  
	//You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANetworkActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANetworkActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
//Overrides GetLifetimeReplicatedProps function of AActor.
void ANetworkActor::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	//calls the macro to always replicate this object
	DOREPLIFETIME(ANetworkActor, OwnerCust);
	//Condition that calls for only simulated instances to be replicated
	DOREPLIFETIME_CONDITION(ANetworkActor, ReplicatedMovement, COND_SimulatedOnly);
}
