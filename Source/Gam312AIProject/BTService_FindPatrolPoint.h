// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Services/BTService_BlackboardBase.h"
#include "BTService_FindPatrolPoint.generated.h"

/**
 * 
 */
UCLASS()
class GAM312AIPROJECT_API UBTService_FindPatrolPoint : public UBTService_BlackboardBase
{
	GENERATED_BODY()
	
public:
	UBTService_FindPatrolPoint();

	//Overrides the BTService TickNode
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	
};
