// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_CheckForPlayer.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "MyAIController.h"
#include "MyPlayerController.h"
#include "Penman.h"
#include "Enemy.h"
#include "AIPatrolPoint.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "DrawDebugHelpers.h"


UBTService_CheckForPlayer::UBTService_CheckForPlayer()
{

	bCreateNodeInstance = true;

}

bool UBTService_CheckForPlayer::canSee(APenman * target, UBehaviorTreeComponent & OwnerComp)
{

	FHitResult Hit;

	//Length of the raycast
	float RayLength = 200;
	AMyAIController *EnemyPC = Cast<AMyAIController>(OwnerComp.GetAIOwner());


	//Starting location of the raycast
	FVector StartLocation = EnemyPC->GetPawn()->GetActorLocation();

	//Ending location of the raycast
	FVector EndLocation = target->GetTargetLocation();

	//Collision parameters. The following syntax means that we don't want the trace to be complex
	FCollisionQueryParams CollisionParameters;

	CollisionParameters.AddIgnoredActor(EnemyPC->GetPawn());

	GetWorld()->LineTraceSingleByChannel(Hit, StartLocation, EndLocation, ECollisionChannel::ECC_WorldStatic, CollisionParameters);

	//DrawDebugLine(GetWorld(), StartLocation, EndLocation, FColor::Green, true, 1, 0, 1.f);

	bool bBlockingHit = target->GetName() == Hit.GetActor()->GetName();



	return bBlockingHit;
}

void UBTService_CheckForPlayer::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	//Get a reference to the AI controller belonging to the parent
	AMyAIController *EnemyPC = Cast<AMyAIController>(OwnerComp.GetAIOwner());

	//Verify AI controller exists to avoid null ref exception
	if (EnemyPC)
	{
		//Get a ref to the player pawn
		APenman *Enemy = Cast<APenman>(GetWorld()->GetFirstPlayerController()->GetPawn());
		AMyPlayerController *PC = Cast<AMyPlayerController>(GetWorld()->GetFirstPlayerController());


		//Verify player (enemy) exists and is in eyesight
		if (Enemy && canSee(Enemy, OwnerComp)) {
			//Sets the value of the EnemyKeyID to the player
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, Enemy);

			if ((EnemyPC->GetPawn()->GetActorLocation() - Enemy->GetActorLocation()).Size() <= 110.0f) {
				//Enemy->GameOver();				
				PC->GameOver();
			}
			}
			else {
				//If the player cannot be seen, go to a patrol point
				TArray<AActor*> FoundPoints;
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIPatrolPoint::StaticClass(), FoundPoints);

				//If destination not already set, or destination is reached
				if (!PatrolPoint || (EnemyPC->GetPawn()->GetActorLocation() - PatrolPoint->GetActorLocation()).Size() <= 110.0f) {
					//set it, if one exists.
					if(FoundPoints[0])
						//Sets the value of the EnemyKeyID to a random point, for now. Later this will have better logic.
					UBTService_CheckForPlayer::PatrolPoint = FoundPoints[rand() % FoundPoints.Num()];
					OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, PatrolPoint);
				}
				else if (FoundPoints[0]) {
					OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, PatrolPoint);
				}
			}	
	}
}
