// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "Penman.h"
#include "BTService_CheckForPlayer.generated.h"

/**
 * 
 */
UCLASS()
class GAM312AIPROJECT_API UBTService_CheckForPlayer : public UBTService
{
	GENERATED_BODY()
	
public:
	AActor* PatrolPoint;

	UBTService_CheckForPlayer();
	//function to determine if AI could realistically detect the player. Simple raycast.
	bool canSee(APenman* target, UBehaviorTreeComponent& OwnerComp);	

	//Overrides the BTService TickNode
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
	
};
