// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "AIController.h"
#include "MyAIController.generated.h"
UCLASS()
class GAM312AIPROJECT_API AMyAIController : public AAIController
{
	GENERATED_BODY()


		UPROPERTY(transient)
		class UBlackboardComponent *BlackboardComp;

	UPROPERTY(transient)
		class UBehaviorTreeComponent *BehaviorComp;
public:


	//declare constructor
	AMyAIController();// const FObjectInitializer& ObjectInitializer);

	//overrides the Possess function
	virtual void Possess(APawn *InPawn) override;

	//Holds the EnemyKeyID from the Blackboard
	uint8 EnemyKeyID;

};
