// Fill out your copyright notice in the Description page of Project Settings.

#include "Penman.h"
#include "Components/InputComponent.h"
#include <string> 
#include "CameraControl.h"
#include "CollidingPawnMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Blueprint/UserWidget.h"
//#include "CameraControl.h"
//Added include of InputComponent.h

// Sets default values
APenman::APenman()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create an instance of our movement component, and tell it to update the root.
	//OurMovementComponent = CreateDefaultSubobject<UCollidingPawnMovementComponent>(TEXT("CustomMovementComponent"));
	//OurMovementComponent->UpdatedComponent = RootComponent;
	//AutoPossessPlayer = EAutoReceiveInput::Player0;//sets the player to posess the inanimate object. "Hey, Hey Morty, look at me. I'M SPHERICAL RIIIIICK."  Sorry.
}

// Called when the game starts or when spawned
void APenman::BeginPlay() 
{	
	Super::BeginPlay();
	
}

// Called every frame
void APenman::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	

}

//While the key bound to the axis in the input (WS,AD) is pressed, the function coorisponding to the bound axis (MoveForward = Lateral, MoveRight = SidetoSide) is called each frame.
void APenman::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{	
	check(InputComponent);
	InputComponent->BindAxis("MoveForward", this, &APenman::Lateral);//Calls the "Lateral" function when the Sprint input binding changes.
	InputComponent->BindAxis("MoveRight", this, &APenman::SidetoSide);//Calls the "SidetoSide" function when the Sprint input binding changes.
	InputComponent->BindAction("Sprint", IE_Pressed, this, &APenman::Sprint);//Calls the "Sprint" function when the Sprint input binding is pressed
	InputComponent->BindAction("Sprint", IE_Released, this, &APenman::Walk);//Calls the "Walk" function when the Sprint input binding is released
	InputComponent->BindAxis("Turn", this, &APenman::Turn);
	InputComponent->BindAxis("Look", this, &APenman::Look);
}
//Called by BindAxis. Presumably the integer value (-1 or 1)
void APenman::Lateral(float Value)
{
	if(Controller && Value)
	{
		//get the forward direction and apply the incremental value for motion, 1 for forwards or -1 for backwards movement.
		//The movement speed is scaled by movementSpeed, which is set to 0.5 by default, and increased to 1 when the Sprint button is held (default is Left hift)
		FVector Forward = GetActorForwardVector();
			AddMovementInput(Forward, Value * movementSpeed);
	}
}
//Renamed from SideSide to SidetoSide
void APenman::SidetoSide(float Value)
{
	if(Controller && Value)
	{
		//get the right direction and apply the incremental value for motion, 1 for right or -1 for leftwards motion.
		FVector Right = GetActorRightVector();
		AddMovementInput(Right, Value * movementSpeed);
	}
}

//If the Sprint binding is currently down, movementSpeed is set to sprintSpeed.
//If the Sprint binding has been released, movement speed is returned to walkSpeed.
void APenman::Sprint() {
	if (Controller) {
		movementSpeed = sprintSpeed;
	}
	else
	{
		movementSpeed = walkSpeed;
	}
}
void APenman::Walk() {
	if (Controller) {
		movementSpeed = walkSpeed;
	}
}
void APenman::Turn(float AxisValue)//Allows the camera to rotate left and right using the mouse.
{
	AddControllerYawInput(AxisValue);	
}
void APenman::Look(float AxisValue)//Allows the camera to look up and down using the mouse.
{
//	USpringArmComponent* boom = Cast<USpringArmComponent>(GetWorld()->GetFirstPlayerController()->GetPawn());
	
	TArray<USpringArmComponent*> boom;
	this->GetComponents<USpringArmComponent>(boom);

	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("World delta for current frame equals %f"), boom[0]->RelativeRotation.Pitch));
	//APenman::boom.
	//limits the camera to a vertical rotation of 90 degrees
	//if ((boom->GetActorRotation().Pitch > -45 || AxisValue >= 0) && (GetActorRotation().Pitch <45 || AxisValue <= 0))
	{
		//rotates entire actor. Only camera should rotate
		if(boom[0] && (boom[0]->RelativeRotation.Pitch > -45 || AxisValue >= 0) && (boom[0]->RelativeRotation.Pitch <0 || AxisValue <= 0))
			boom[0]->AddLocalRotation(FRotator(AxisValue, 0, 0));//same as above, but easier
	}
}
