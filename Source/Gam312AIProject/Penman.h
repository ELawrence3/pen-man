// Fill out your copyright notice in the Description page of Project Settings.
//Created by Evan Lawrence
//GAM-312-Q6158 18EW6
//1-2 Guided Tutorial 1: Display and Animate a Character
//As this is for a 3D game, the character will be projected using perspective, rather than orthographic projection.
//This method allows the camera and therefore player to differentiate between objects that are far and near to the camera.

#pragma once

#include "Core.h"
#include "GameFramework/Character.h"
#include "UserWidget.h"
#include "Penman.generated.h"

UCLASS()
class GAM312AIPROJECT_API APenman : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APenman();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementSpeed)
	float walkSpeed = 0.25f;//Default speed when walking
	//Default speed when Sprint binding is held down. 
	//Appears to be considered a percentage, so sprintSpeed has a max of 1. The actual maximum speed can be configured from the CharacterMovement component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementSpeed)
	float sprintSpeed = 1;
	float movementSpeed = walkSpeed;//Current speed
	int alive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Widgets)
		TSubclassOf<class UUserWidget> GameOverMenu;
	

	

public:	
	class UCollidingPawnMovementComponent* OurMovementComponent;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	void Lateral(float Value); //Forward and Backward
	void SidetoSide(float Value); //Right and Left	
	void Sprint();//toggle sprint
	void Walk();//toggle sprint
	void Turn(float AxisValue);
	void Look(float AxisValue);	

	
};
