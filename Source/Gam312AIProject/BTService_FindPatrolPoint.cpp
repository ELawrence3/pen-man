// Fill out your copyright notice in the Description page of Project Settings.

#include "BTService_FindPatrolPoint.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "MyAIController.h"
#include "Penman.h"
#include "Enemy.h"
#include "AIPatrolPoint.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"



UBTService_FindPatrolPoint::UBTService_FindPatrolPoint()
{

	bCreateNodeInstance = true;

}

void UBTService_FindPatrolPoint::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	//Get a reference to the AI controller belonging to the parent
	AMyAIController *EnemyPC = Cast<AMyAIController>(OwnerComp.GetAIOwner());

	//Verify AI controller exists to avoid null ref exception
	if (EnemyPC)
	{
		//Get a ref to the player pawn
		//APenman *Enemy = Cast<APenman>(GetWorld()->GetFirstPlayerController()->GetPawn());

		TArray<AActor*> FoundPoints;

		UGameplayStatics::GetAllActorsOfClass(GetWorld(), AAIPatrolPoint::StaticClass(), FoundPoints);


		//for (TActorIteratorBase<AAIPatrolPoint> Itr; Itr; ++Itr)
		{

			//	AAIPatrolPoint *Enemy = *Itr;

			if (FoundPoints[0]) {
				//Sets the value of the EnemyKeyID to the player
				OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->EnemyKeyID, FoundPoints[0]);
				//break;
			}
		}

		//Verify player (enemy) exists

	}
}



