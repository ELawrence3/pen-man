// Fill out your copyright notice in the Description page of Project Settings.

#include "MyPlayerController.h"
#include "Penman.h"
#include "Runtime/UMG/Public/Blueprint/UserWidget.h"





void AMyPlayerController::BeginPlay()
{
	Super::BeginPlay();
	active = true;
	//MyMenu functionality moved to Menu world BP to prevent main menu from loading on world change
	/*if (wMainMenu) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		MyMenu = CreateWidget<UUserWidget>(this, wMainMenu);

		// now you can use the widget directly since you have a referance for it.
		// Extra check to make sure the pointer holds the widget.
		if (MyMenu)
		{
			//let add it to the view port
			MyMenu->AddToViewport();
		}
		//Show the Cursor.
		bShowMouseCursor = true;
	}*/
}

void AMyPlayerController::GameOver() {
	
	if (wGameOverMenu) // Check if the Asset is assigned in the blueprint.
	{
		//Create widget, get reference
		MyGameOverMenu = CreateWidget<UUserWidget>(this, wGameOverMenu);

		APenman *player = Cast<APenman>(this->GetCharacter());

		if (MyGameOverMenu && player && active == true) {
			active = false;
			//Make it visible
			MyGameOverMenu->AddToViewport();			
			//Game over, player walks no more
			player->DisableInput(Cast<APlayerController>(this));			
		}
	}
}
