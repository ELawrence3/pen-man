// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MyPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GAM312AIPROJECT_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
		// Reference UMG Asset in the Editor
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wGameOverMenu;
	
	// Variable to hold the widget After Creating it.
	UUserWidget* MyGameOverMenu;
	// Override BeginPlay()
	virtual void BeginPlay() override;
	UFUNCTION(BlueprintCallable, Category = "Victory BP Library", meta = (WorldContext = "WorldContextObject"))
	virtual void GameOver();

	bool active = true;
};
