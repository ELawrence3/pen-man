// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Core.h"
#include "GameFramework/Pawn.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ParticleHelper.h"
#include "Particles/ParticleSystem.h"
#include "Particles/ParticleSystemComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "ComponentPawn.generated.h"

UCLASS()
class GAM312AIPROJECT_API AComponentPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AComponentPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OnHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual UPawnMovementComponent* GetMovementComponent() const override;//Why though?  I suppose if we made the movement component object private?
	UParticleSystemComponent *OurParticleSystem;//Just a bit of optional flare
	class UCollidingPawnMovementComponent* OurMovementComponent;//Added custom movement component
	USphereComponent* Sphere;

	TArray<AActor*> heldActors;
	//USphereComponent* SphereCollider;//Added additional collider. One handles collisions while other handles overlap events. Removed as it is not required for this assignment
	
	//movement class definitions
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void Turn(float AxisValue);
	void Look(float AxisValue);
	void ParticleToggle();
	//Added function for dropping picked up actors
	void Release();
	
};
